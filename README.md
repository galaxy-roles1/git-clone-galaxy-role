Install role 
------------
```yaml
- name: git_clone
  src: git+https://gitlab.com/galaxy-roles1/git-clone-galaxy-role.git
  version: v1  
```

Example Playbook
----------------
```yaml
- hosts: servers
  gather_facts: true
  become: true
  roles:
    - git_clone
```

